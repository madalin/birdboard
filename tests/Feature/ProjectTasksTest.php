<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Facades\Tests\Setup\ProjectFactory;

use App\Project;
use App\Task;

class ProjectTasksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function guests_cannot_add_tasks_to_projects()
    {
        $project = factory('App\Project')->create();

        $this->post($project->path() . '/tasks')->assertRedirect('login');
    }

    /** @test */
    public function only_the_owner_of_a_project_may_add_tasks()
    {
        $this->signIn();

        $project = factory(Project::class)->create();

        $task = factory(Task::class)->raw();

        $this->post($project->path() . '/tasks', $task)->assertStatus(403);

        $this->assertDatabaseMissing('tasks', $task);
    }

    /** @test */
    public function a_project_can_have_tasks()
    {
        // create me a user
        $this->signIn();

        // create a project for me user
        $project = factory('App\Project')->create(['owner_id' => auth()->id()]);

        $task = $this->faker->sentence;

        $this->post($project->path() . '/tasks', ['body' => $task]);

        $this->get($project->path())->assertSee($task);
    }

    /** @test */
    public function only_the_owner_of_a_project_may_update_a_task()
    {
        $this->signIn();

        $project = ProjectFactory::withTasks(1)
            ->create();

        $this->patch($project->tasks->first()->path(), [
            'body' => 'changed'
        ])->assertStatus(403);

        $this->assertDatabaseMissing('tasks', [
            'body' => 'changed'
        ]);
    }

    /** @test */
    public function a_task_can_be_updated()
    {

        $project = ProjectFactory::ownedBy($this->signIn())
            ->withTasks(1)
            ->create();

        $this->patch($project->tasks->first()->path(), $changed_task = [
            'body' => $this->faker->sentence,
            'completed' => true
        ]);

        $this->assertDatabaseHas('tasks', $changed_task);
    }

    /** @test */
    public function a_requires_a_body()
    {
        // create me a user
        $this->signIn();

        // create a project for me user
        $project = factory('App\Project')->create(['owner_id' => auth()->id()]);

        $task = factory('App\Task')->raw(['body' => '']);

        $this->post($project->path() . '/tasks', $task)->assertSessionHasErrors('body');
    }
}
