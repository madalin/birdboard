<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

use App\User;

/** @var Factory $factory */
$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'notes' => $faker->sentence,
        'owner_id' => factory(User::class),
    ];
});
